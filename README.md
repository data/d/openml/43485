# OpenML dataset: Dollar-Stock-Prices-and-infos

https://www.openml.org/d/43485

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
To build a AI to predict the stock price of the Dollar currency on IBOVESPA I had to make this dataset.
All information collected here is from a standard graphic for stock prices.
Content
The data is organized by prices and infos per minute.
Each row contains:
date, open price, maximim value, minimum value, close price, volume, financial, negotiations, mme13, mme72, high mean, low mean ,diffMACD, deaMACD, MACDlh, difflh, dealh, target.
The target columns is the price 15 minutes in the future.
Inspiration
This data is here to construct an Artificial Intelligence to predict the price in 15 minutes.
We want to buy if the predicted price is above of your goal per trade (mine goal is 8 point per trade) and sell if the prediction is below of your goal.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43485) of an [OpenML dataset](https://www.openml.org/d/43485). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43485/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43485/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43485/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

